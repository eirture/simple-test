package com.eirture.work

/**
 * Created on 2017/12/28.
 *
 * @author Jie Liu
 */

fun main(args: Array<String>) {
    val compressor = PIGZCompressor()
    compressor.work(arrayOf("pigz", "-dkf", "/Users/eirture/Downloads/test_4.fastq.gz"))
}

fun testCompress() {
    val compressor = GZIPProcessCompressor()
    val beginTime = System.currentTimeMillis()
    compressor.compress("/Users/eirture/Downloads/test_5.fastq", "out/test_5.fq.gz")
    val spend = System.currentTimeMillis() - beginTime
    println("PIGZ compress spend: $spend.")
}