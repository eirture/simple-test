package com.eirture.work

import com.google.common.io.Files
import java.io.File
import java.io.IOException

/**
 * Created on 2017/12/28.
 *
 * @author Jie Liu
 */
class GZIPProcessCompressor : Compressor {
    companion object {
        private val PROCESSOR: String = "processor/pigz_mac"
    }

    override fun compress(srcPath: String?, targetPath: String?) {
        val processBuilder = ProcessBuilder(listOf(PROCESSOR, "-kf", "--fast", srcPath))
        val p = processBuilder.start()
        val exitCode = p.waitFor()
        if (exitCode != 0) {
            throw IOException("${javaClass.simpleName} compress failed! exit code is '$exitCode'")
        }
        val resultFilePath = "$srcPath.gz"
        if (targetPath != resultFilePath) {
            Files.move(File(resultFilePath), File(targetPath))
        }
    }

    override fun decompress(srcPath: String, targetPath: String) {
        var inputPath = srcPath
        if (!inputPath.endsWith(".gz")) {
            val newSrcPath = "$srcPath.gz"
            Files.move(File(srcPath), File(newSrcPath))
            inputPath = newSrcPath
        }
        val processBuilder = ProcessBuilder(listOf(PROCESSOR, "-dkf", "--fast", inputPath))
        val p = processBuilder.start()
        val exitCode = p.waitFor()
        if (exitCode != 0) {
            throw IOException("${javaClass.simpleName} compress failed! exit code is '$exitCode'")
        }
    }

}