package com.eirture.work;

/**
 * Created on 2017/12/29.
 *
 * @author Jie Liu
 */
public class PIGZCompressor {

    static {
        System.load("/Users/eirture/GeneDock/Java/simple-test/src/main/resources/pigz-compressor.jnilib");
    }

    public native int work(String[] argv);
}
