package com.eirture.work;

/**
 * Created on 2017/12/28.
 *
 * @author Jie Liu
 */
public interface Compressor {

    void compress(String srcPath, String targetPath);

    void decompress(String srcPath, String targetPath);
}
